class ArtistService
  include HTTParty
  base_uri 'https://www.moat.ai'
  DEFAULT_HEADERS = {Basic: "ZGV2ZWxvcGVyOlpHVjJaV3h2Y0dWeQ=="}
  
  def self.get_artists
    response = get("/api/task", { headers: DEFAULT_HEADERS })
    JSON.parse(response&.body || "{}", symbolize_names: true)
  end
end