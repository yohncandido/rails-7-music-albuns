module AlbumsHelper
  def artists_select_options
    ArtistService.get_artists.map do |artist|
      [
        artist[0][:name],
        artist[0][:id]
      ]
    end
  end
end
