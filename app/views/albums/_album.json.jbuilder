json.extract! album, :id, :artist_external_id, :album_name, :year, :created_at, :updated_at
json.url album_url(album, format: :json)
